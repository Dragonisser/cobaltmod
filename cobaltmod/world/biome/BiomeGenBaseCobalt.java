package cobaltmod.world.biome;

import net.minecraft.world.biome.BiomeGenBase;
import cobaltmod.main.CMMain;
import cpw.mods.fml.common.FMLLog;

public class BiomeGenBaseCobalt extends BiomeGenBase
{

	/** The biome height */
	//protected static final BiomeGenBase.Height height_pForest = new BiomeGenBase.Height(0.3F, 0.6F); 
	
	/** The biome decorator. */

    public static BiomeGenBase biomeplains;
    public static BiomeGenBase biomehills;
    public static BiomeGenBase biomeswamp;
    public static BiomeGenBase biometall;
	
	public BiomeGenBaseCobalt(int biomeId) 
	{
		super(biomeId);
		//init();
		//registerWithBiomeDictionary();
		this.rainfall = 0.6F;
	}
	
    public static boolean checkForBiomeConflicts()
    {
        boolean noConflict = true;
        noConflict &= checkBiomeIDisClearOrOurs(CMMain.biomehillsid);
        noConflict &= checkBiomeIDisClearOrOurs(CMMain.biomeplainsid);
        noConflict &= checkBiomeIDisClearOrOurs(CMMain.biomeswampid);
        noConflict &= checkBiomeIDisClearOrOurs(CMMain.biometallid);

        if (!noConflict)
        {
            FMLLog.warning("[CobaltMod] Biome ID conflict detected.  Edit the CobaltMod config to give all biomes unique IDs.", new Object[0]);
        }

        return noConflict;
    }
    
    public static boolean checkBiomeIDisClearOrOurs(int id)
    {
    	BiomeGenBase biomeAt = BiomeGenBase.getBiome(id);

        if (biomeAt == null || (biomeAt instanceof BiomeGenBaseCobalt))
        {
            return true;
        }
        else
        {
            FMLLog.warning("[CobaltMod] Biome ID conflict.  Biome ID %d contains a biome named %s, but CobaltMod is set to use that ID.", new Object[]
                           {
                               Integer.valueOf(id), biomeAt.biomeID
                           });
            return false;
        }
    }
    
    public static void init()
    {		
    	biomehills = (new BiomeGenCobaltHills(CMMain.biomehillsid)).setBiomeName("Blue Mountains");
	  	biomeplains = (new BiomeGenCobaltPlains(CMMain.biomeplainsid)).setBiomeName("Cobex Forest");
	  	biomeswamp = (new BiomeGenCobaltSwamp(CMMain.biomeswampid)).setBiomeName("Deep Swamp");
	  	biometall = (new BiomeGenCobaltTall(CMMain.biometallid)).setBiomeName("Tall Forest");
	  	registerWithBiomeDictionary();
    }
    
    public static void registerWithBiomeDictionary()
    {
    	//BiomeDictionary.registerBiomeType(biomeplains, Type.FOREST, Type.PLAINS, Type.HOT);
	  	
	  	//BiomeDictionary.registerBiomeType(biomeswamp, Type.FOREST, Type.PLAINS, Type.HOT);
	  	
	  	//BiomeDictionary.registerBiomeType(biomehills, Type.MOUNTAIN, Type.FOREST, Type.HILLS);
        
	  	//BiomeDictionary.registerBiomeType(biometall, Type.FOREST, Type.PLAINS, Type.HOT);
	  	
        //BiomeDictionary.registerAllBiomes();
    }
    

    /*@Override
    public BiomeDecorator createBiomeDecorator()
    {
        return getModdedBiomeDecorator(new PFBiomeDecorator(this));
    }*/

    /**
     * Gets a WorldGen appropriate for this biome.
     */
    /*public WorldGenerator getRandomWorldGenForGrass(Random par1Random)
    {
        return new WorldGenTallGrass(PFBlocks.silverTallGrass.blockID, 1);
    }*/
}